[![build status](https://gitlab.com/katalysis/KeyGenerator/badges/master/build.svg)](https://gitlab.com/katalysis/KeyGenerator/commits/master)

**KeyGenerator project**

This is the companion code to a medium post from www.katalysis.io.

It has been tested for Swift 3.1 on MacOS and Linux.
