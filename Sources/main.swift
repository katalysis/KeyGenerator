
//
//  Created by Alex Tran-Qui on 01/05/2017.
//  Copyright © 2017 Katalysis (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

import ErisKeys
#if os(OSX) || os(iOS) || os(tvOS) || os(watchOS)
    import Darwin
#elseif os(Linux)
    import Glibc
#endif

public func localRand() -> UInt8? {
    #if os(Linux) || os(Android) || os(FreeBSD)
        let fd = open("/dev/urandom", O_RDONLY)
        if fd <= 0 {
            return nil
        }
        
        var value: UInt8 = 0
        let result = read(fd, &value, MemoryLayout<UInt8>.size)
        precondition(result == 1)
        
        close(fd)
        return value
    #else
        return UInt8(arc4random_uniform(UInt32(UInt8.max) + 1))
    #endif
}


var seed = [UInt8]()
for i in 0..<32 {
    if let r = localRand() {
        seed.append(r)
    }
}

if let key = ErisKey(seed: seed) {
    
    let seedStr: String = seed.reduce("") {(s,u) in return s + String(format: "%02X",u)}
    
    let pubKey = key.pubKey
    
    print("Seed      : \(seedStr)")
    print("Public key: \(key.pubKeyStr)")
    print("Account   : \(key.account)")
    
    
    let messageStr = "Hello Marmots!"
    print("Message to sign: \(messageStr)")
    
    let message = [UInt8](messageStr.utf8)
    let signature = key.sign(message)
    let sigStr: String = signature.reduce("") {(s,u) in return s + String(format: "%02X",u)}
    
    print("Signature      : \(sigStr)")
    
    if (ErisKey.verify(pubKey, message, signature)) {
        print("Message has been verified!")
    } else {
        print("Message has been tampered with!")
    }
} else {
    print("Seed is not 32 bytes")
}
