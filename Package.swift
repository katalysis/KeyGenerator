// swift-tools-version:3.1

import PackageDescription
let package = Package(
  name: "KeyGenerator",
  dependencies: [
    .Package(url: "https://gitlab.com/katalysis/ErisKeys.git", majorVersion: 0, minor: 3),
]
)
